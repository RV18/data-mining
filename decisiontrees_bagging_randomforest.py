# Bag of Words - NBC
# import statements
import pandas as pd
import numpy as np
from collections import Counter
import string
import sys
from numpy import linalg as LA
from math import sqrt
from random import randrange

# np.set_# printoptions(threshold=sys.maxsize)

S1 = pd.DataFrame([])
S2 = pd.DataFrame([])
S3 = pd.DataFrame([])
S4 = pd.DataFrame([])
S5 = pd.DataFrame([])
S6 = pd.DataFrame([])
S7 = pd.DataFrame([])
S8 = pd.DataFrame([])
S9 = pd.DataFrame([])
S10 = pd.DataFrame([])
maxdepth = 0

def process_str(s):
    rem_punc = str.maketrans('', '', string.punctuation)
    return s.translate(rem_punc).lower().split()

def read_dataset(file_name):
    dataset = []
    with open(file_name) as f:
        for line in f:
            index, class_label, text = line.strip().split('\t')
            words = process_str(text)
            dataset.append( (int(class_label), words) )

    return dataset

# Function to preprocess the data
def pre_process(rText):
    newrText = []
    for i in range(len(rText)):
        newrText.append(str(rText[i]).lower())
        translator = str.maketrans('', '', string.punctuation)
        newrText[i] = newrText[i].translate(translator)
        newrText[i] = newrText[i].split()
    return newrText


def get_most_commons(dataset, skip=100, total=100):
    counter = Counter()
    for item in dataset:
        counter = counter + Counter(set(item[1]))

    temp = counter.most_common(total+skip)[skip:]
    words = [item[0] for item in temp]
    return words


def generate_vectors(dataset, common_words):
    d = {}
    for i in range(len(common_words)):
        d[common_words[i]] = i

    vectors = []
    for item in dataset:
        vector = [0] * len(common_words)
        for word in item[1]:
            if word in d:
                vector[d[word]] = 1

        vector.append(item[0]) # Added class label to the end of the feature vector
        vectors.append(vector)
    return vectors


# Function to find and count unique words
def count_unique_words(new_review_text):
    import collections
    c = collections.Counter()
    # # print("Initial :", c)
    for review in new_review_text:  # TO DO - should newReviewText be populated with the no dup values?
        review_nodup = {word for word in review}
        c.update(review_nodup)
    # # print("Final :", c)
    return c


# Function to sort and discard top n words
def sort_and_discard(wordFreqTable, n):
    # # print("Before discarding top 100: \n", wordFreqTable)
    discard_count = 0
    for k, v in wordFreqTable.most_common():
        if discard_count < n:
            del wordFreqTable[k]
            discard_count += 1
    # # print("After discarding top 100: \n",wordFreqTable)
    return wordFreqTable


# Function to construct features for next 500 words
def construct_features(features_set, wordFreqTable500, review_text_set, model):
    # feature_words = open('feature_words.txt', 'w')
    # # print(wordFreqTable500)
    i, j = 0, 0
    for review in review_text_set:
        # # print(type(review), review)
        for k, v in wordFreqTable500:
            if k in review:
                # if review.count(k) == 1:
                features_set[i, j] = 1
                # else:
                #     features_set[i, j] = 2
                # feature_words.write(k + " ")
            j += 1
        i += 1
        j = 0
        # feature_words.write("\n")
    # feature_words.close()
    if model != 0:                                     # Adding intercept for LR and SVM
        intercept = np.ones((features_set.shape[0], 1))
        features = np.hstack((intercept, features_set))
    return features_set


# Function to construct features for next 500 words
def construct_features_file(filename, features_set, wordFreqTable500, review_text_set, columns):
    # feature_words = open(filename, 'w')
    # # print(wordFreqTable500)
    i, j = 0, 1  # First cell (i, 0) is for intercept
    for review in review_text_set:
        features_set[i, 0] = 1
        # # print(type(review), review)
        for k, v in wordFreqTable500:
            if k in review:
                features_set[i, j] = 1
                # feature_words.write(k + " ")
            j += 1
        i += 1
        j = 1
        # feature_words.write("\n")
    # feature_words.close()
    return features_set


# Function - Write feature vector to file
def features_to_file(filename, features):
    target = open(filename, 'w')
    for line in features:
        target.write(" ".join(str(elem) for elem in line) + "\n")
    target.close()


# Function - # print top ten selected words - words 101-110
def print_top_ten(wordFreqTable500):
    counter_ten = 1
    for k, v in wordFreqTable500:
        # print("WORD%d %s" % (counter_ten, k))
        counter_ten += 1
        if counter_ten > 10:
            break
    return None


# Function to count number of positive, negative and total reviews in training data
def count_number_reviews(list_label):
    return sum(list_label), len(list_label) - sum(list_label), len(list_label)


# Function to calculate P(0) and P(1)
def gini_index(set):
    gini = 0.0
    size = len(set)
    # # print("size is", size)
    if size == 0:
        return 0
    # # print([row[-1] for row in set])
    # # print([row[-1] for row in set].count(0))
    zeroes = [row[-1] for row in set].count(0) / float(size) # p(x) for x = 0
    # # print(zeroes)
    gini = 1 - (zeroes * zeroes) - ((1.0 - zeroes) * (1.0 - zeroes)) # 1 - (sigma) p(x) for x = 0, 1
    return gini


def set_split(index, set):
    set_0, set_1 = list(), list()
    for row in set:
        # # print(row[index])
        if row[index] == 0:
            set_0.append(row)
        else:
            set_1.append(row)
    # # print("one split done:", strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    return set_0, set_1


# def get_split(set):
#     # class_labels = list(set(row[-1] for row in set))    # Change to class labels
#     best_index, max_gain, best_left_tree, best_right_tree = 0, 0, None, None
#     gini_S = gini_index(set)
#     size_S = len(set)
#     # # print(gini_S, "and size of set is ", size_S)
#     for feature_index in range(len(set[0]) - 1):
#         for row in set:
#             left_sub_tree, right_sub_tree = set_split(feature_index, set)
#             gini_left = gini_index(left_sub_tree)
#             gini_right = gini_index(right_sub_tree)
#             gain = gini_S - (len(left_sub_tree) * gini_left / size_S) - (len(right_sub_tree) * gini_right / size_S)
#             if gain >= max_gain:
#                 best_index, max_gain, best_left_tree, best_right_tree = feature_index, gain, left_sub_tree, right_sub_tree
#     return best_index, best_left_tree, best_right_tree


def get_split(set):
    global model_idx
    no_of_features = 1000
    best_index, max_gain, best_left_tree, best_right_tree = 0, 0, (), ()
    gini_S = gini_index(set)
    size_S = len(set)
    # # print(size_S)
    # # print(len(set[0]))

    if model_idx == 3:
        features = list()
        while len(features) < round(sqrt(no_of_features)):
            i = randrange(len(set[0]) - 2)
            if i not in features:
                features.append(i)
        # # print("should not be here")
    else:
        features = np.arange(len(set[0]) - 2)
        # # print("# printing features", features)
    # # print(len(set[0]) - 1, "lennn set 0")
    # # print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    for feature_index in features:
        # for row in set:
        # # print(feature_index, "feature index")
        left_sub_tree, right_sub_tree = set_split(feature_index, set)
        gini_left = gini_index(left_sub_tree)
        gini_right = gini_index(right_sub_tree)
        gain = gini_S - (len(left_sub_tree) * gini_left / size_S) - (len(right_sub_tree) * gini_right / size_S)
        if gain >= max_gain:
            best_index, max_gain, best_left_tree, best_right_tree = feature_index, gain, left_sub_tree, right_sub_tree
    # # print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    # # print(best_index, best_left_tree, best_right_tree)
    # # print(type(best_left_tree),"&&&&&&")
    # if (best_left_tree) == None):
    #     # print("in here")
    #     best_left_tree = ()
    # elif (type(best_right_tree) == None):
    #     best_right_tree = ()
    return best_index, best_left_tree, best_right_tree


def split(current, depth):
    global maxdepth
    left_child, right_child = current['children']
    # # print(current)
    # print("No of examples in left tree - ", len(left_child))
    # print("No of examples in right tree - ", len(right_child))
    current['children'] = ()
    if not left_child or not right_child:
        # # print(type(current['right_child']))
        # # print(type(current['left_child']))
        # # print(type(current['left_child'] + current['right_child']))
        # # print("0---")
        current['left_child'] = set_label(left_child + right_child)
        current['right_child'] = current['left_child']
        return
    if depth >= maxdepth:
        current['left_child'] = set_label(left_child)
        # # print(current['left_child'])
        current['right_child'] = set_label(right_child)
        # # print(current['right_child'])
        # print("in depth", depth)
        return
    if len(left_child) < 10:
        current['left_child'] = set_label(left_child)
        # # print("1---")
    else:
        # print("2---start")
        index, left_tree, right_tree = get_split(left_child)
        current['left_child'] = {'index': index, 'children': (left_tree, right_tree)}
        # print(index, "----", type(current['left_child']))
        # # print("*******************")
        # # print(type(current['left_child']))
        # exit()
        split(current['left_child'], depth + 1)
        # print("2--- end")
    # process right child
    if len(right_child) < 10:
        # print("3---start")
        current['right_child'] = set_label(right_child)
        # print("3---end")
    else:
        # print("4--- start", depth)
        index, left_tree, right_tree = get_split(right_child)
        # current['right_child'] = {'index': index, 'left_child': left_tree, 'right_child': right_tree}
        current['right_child'] = {'index': index, 'children': (left_tree, right_tree)}
        # print(index, "----", type(current['left_child']))
        split(current['right_child'], depth + 1)
        # print("4---end")
        return
    return

def set_label(set):
    class_labels = [row[-1] for row in set]
    return max(0, 1, key=class_labels.count)


def learn_decision_trees(train_vector):
    index, left_tree, right_tree = get_split(train_vector)
    # print("best index - in learn decision trees", index)
    # # print("No of examples in left tree - ", len(left_tree))
    # # print("No of examples in right tree - ", len(right_tree))
    root = {'index': index, 'children': (left_tree, right_tree)}
    split(root, 1)
    return root


def decision_tree_prediction(current, row):
    if row[current['index']] == 0:
        if isinstance(current['left_child'], dict):
            return decision_tree_prediction(current['left_child'], row)
        else:
            return current['left_child']
    else:
        if isinstance(current['right_child'], dict):
            return decision_tree_prediction(current['right_child'], row)
        else:
            return current['right_child']


def apply_decision_tree(model, test):
    predicted_labels = list()
    for row in test:
        label = decision_tree_prediction(model, row)
        predicted_labels.append(label)
    return predicted_labels


def build_sample(set):
    sample = list()
    sample_size = len(set)
    while len(sample) < sample_size:
        index = randrange(sample_size)
        sample.append(set[index])
    return sample


def learn_bagging(train):
    bagged_trees = [learn_decision_trees(build_sample(train)) for i in range(50)]
    return bagged_trees


def apply_bagging(models, test):
    predictions = list()
    # # print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    for row in test:
        predictions_per_row = [decision_tree_prediction(model, row) for model in models]
        predictions.append(max(0, 1, key=predictions_per_row.count))
    # # print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    return predictions



# Function - evaluate zero-one loss
def calc_error(pred, labels):
    error = sum(np.where(pred != labels, 1, 0))
    return (error / labels.size)

def evaluate_loss(expected, predicted):
    diff = (expected == predicted)
    # print(diff)
    # # print("No. of mismatches in expected and predicted: ", len(expected)- diff.sum())
    return 1 - (diff.sum() / len(expected))


# def pick_random_sample(percent):
#     a = np.arange(len(yelp_data))                                   # numbers from 0 to len
#     percentage = percent / 100
#     no_rows_int = int(percentage * len(yelp_data))                  # No of rows picked for training
#     # # print("No. of rows picked for training: ",no_rows_int)
#     train_indices = np.random.choice(a, no_rows_int, replace=False) # Picking n ind randomly from a
#     # # print("train indices are ", train_indices)
#     zero_array = np.zeros(len(yelp_data), dtype=bool)
#     zero_array[train_indices] = True                                # Bool array with train ind set to true
#     train = yelp_data[zero_array]                                   # Picking those for training
#     test  = yelp_data[~zero_array]                                  # Picking remaining for testing
#     # # print("After random sampling: ", zero_array)
#     # # print("train data ", train)
#     return train, test


def change_label_to_minus(y_actual):
    y_labels = np.array(y_actual)
    y_labels[y_labels < 1] = -1
    return y_labels





def common_to_models(columns, text_for_training, text_for_testing, model):
    wordTable = count_unique_words(text_for_training)   # Counter
    wordTable = sort_and_discard(wordTable, 100)        # Sort and discard top 100
    columns += 1                                        # No of features - added 1 for intercept
    rows = len(text_for_training)
    wordTable500 = wordTable.most_common(columns)
    training_features = np.array([[0 for x in range(columns)] for y in range(rows)])
    training_features = construct_features(training_features, wordTable500, text_for_training, model)
    rows = len(text_for_testing)
    test_features = np.array([[0 for x in range(columns)] for y in range(rows)])
    test_features = construct_features(test_features, wordTable500, text_for_testing, model)
    # # print(training_features, test_features)
    return training_features, test_features


def learn_SVM(number, columns, training_features, y_labels):
    w = np.zeros(columns, dtype=np.longfloat)
    N = len(training_features)
    gradient_2d = np.zeros((N, columns), dtype=np.longfloat)
    w_new = np.zeros(columns, dtype=np.longfloat)
    gradient = np.zeros(columns, dtype=np.longfloat)

    iterations = 0
    while iterations < 100:
        y_predictions = np.dot(training_features, w)
        # Make predictions
        y_predictions[y_predictions >= 0] = 1
        y_predictions[y_predictions < 0] = -1
        # Calculate gradient
        y_product = y_labels * y_predictions
        indices = np.where(y_product >= 1)[0]
        # # print(indices)
        for j in range(0, columns):
            s = 0
            w[j] = w_new[j]
            # for i in indices:
            #     gradient_2d[i,j] = y_labels[i] * training_features[i, j]
            gradient_2d[:,j] = y_labels * training_features[:,j]
            for i in indices:
                gradient_2d[i,j] = 0
            sum = gradient_2d[:,j].sum()
            gradient = (0.01 * w[j]) - (sum / N)
            w_new[j] = w[j] - 0.5 * gradient
        tol = LA.norm(w_new - w)                                              # L2 norm
        # # print("tol is ", tol)
        if tol <= 0.000001:                                                   # Check tolerance
            # # print("am i here?")
            break
        iterations += 1
    # # print("No of iterations ", iterations)
    # # print(w_new)
    return w_new


def apply_SVM(test_features, w):
    # # print(test_features.shape)
    # # print(w.shape)
    y_predictions = np.dot(test_features, w)
    # Make predictions
    y_predictions[y_predictions >= 0] = 1
    y_predictions[y_predictions < 0] = -1
    return y_predictions


def partition_into_disjoint_sets(dataset):
    # np.random.shuffle(dataset)
    # subset = dataset.sample(frac=0.8)
    # remaining = dataset.drop(subset['reviewID'])
    a = np.arange(len(dataset))                                         # numbers from 0 to len
    no_rows_int = 200                                                   # No of rows picked for a set S
    subset_indices = np.random.choice(a, no_rows_int, replace=False)    # Picking n ind randomly from a
    zero_array = np.zeros(len(dataset), dtype=bool)
    zero_array[subset_indices] = True                                   # Bool array with train ind set to true
    subset = dataset[zero_array]                                        # Picking those for training
    remaining = dataset[~zero_array]                                    # Picking remaining
    return subset, remaining

def f(x):
    return {
        1: S1,
        2: S2,
        3: S3,
        4: S4,
        5: S5,
        6: S6,
        7: S7,
        8: S8,
        9: S9,
        10: S10,
    }[x]

def get_Sc(idx):
    Sc = pd.DataFrame([])
    for i in range(1, 11):
        if i != idx:
            Sc = Sc.append(f(i))
    return Sc

def make_sets(yelp_data):
    # Partition dataset into 10 disjoint sets
    global S1, S2, S3, S4, S5, S6, S7, S8, S9, S10
    S1, remaining_main = partition_into_disjoint_sets(yelp_data)
    S2, remaining_main = partition_into_disjoint_sets(remaining_main)
    S3, remaining_main = partition_into_disjoint_sets(remaining_main)
    S4, remaining_main = partition_into_disjoint_sets(remaining_main)
    S5, remaining_main = partition_into_disjoint_sets(remaining_main)
    S6, remaining_main = partition_into_disjoint_sets(remaining_main)
    S7, remaining_main = partition_into_disjoint_sets(remaining_main)
    S8, remaining_main = partition_into_disjoint_sets(remaining_main)
    S9, remaining_main = partition_into_disjoint_sets(remaining_main)
    S10, remaining_main = partition_into_disjoint_sets(remaining_main)


def learn_models(train_set, test_set, model, number):
    features_train, features_test = common_to_models(number, train_set["reviewText"], test_set["reviewText"], model)
    if model == 1:
        tree = learn_decision_trees(features_train)
        # # print_tree(tree)  # TO DO - remove
        predicted_class_labels = apply_decision_tree(tree, features_test)
        loss_dt = evaluate_loss(np.array(test_set['classLabel']), predicted_class_labels)
        # print('ZERO-ONE-LOSS-DT', loss_dt)
        return loss_dt
    elif model == 2:
        trees = learn_bagging(features_train)
        predicted_class_labels = apply_bagging(trees, features_test)
        loss_bt = evaluate_loss(np.array(test_set['classLabel']), predicted_class_labels)
        # print('ZERO-ONE-LOSS-BT', loss_bt)
        return loss_bt
    elif model == 3:
        trees = learn_bagging(features_train)
        predicted_class_labels = apply_bagging(trees, features_test)
        loss_rf = evaluate_loss(np.array(test_set['classLabel']), predicted_class_labels)
        # print('ZERO-ONE-LOSS-RF', loss_rf)
        return loss_rf
    elif model == 4:
        weights = learn_SVM("Q1", len(features_train[0]), features_train,
                            change_label_to_minus(train_set['classLabel']))
        # # print(weights)
        predicted_class_labels = apply_SVM(features_test, weights)
        zero_one_loss_svm = evaluate_loss(change_label_to_minus(test_set['classLabel']), predicted_class_labels)
        # print("ZERO-ONE-LOSS-SVM  ", zero_one_loss_svm)
        return zero_one_loss_svm
    else:
        # print('Illegal modelIdx')
        return 0.0

def analysis1(yelp_data):
    global model_idx
    make_sets(yelp_data)
    proportions = np.array([0.025, 0.05, 0.125, 0.25])
    sizes = 2000 * proportions
    dt_av = np.zeros(4, dtype=float)
    dt_ste = np.zeros(4, dtype=float)
    bt_av = np.zeros(4, dtype=float)
    bt_ste = np.zeros(4, dtype=float)
    rf_av = np.zeros(4, dtype=float)
    rf_ste = np.zeros(4, dtype=float)
    svm_av = np.zeros(4, dtype=float)
    svm_ste = np.zeros(4, dtype=float)
    i = 0

    for TSS in sizes:
        dt_loss = np.zeros(10, dtype=float)
        bt_loss = np.zeros(10, dtype=float)
        rf_loss = np.zeros(10, dtype=float)
        svm_loss = np.zeros(10, dtype=float)
        for idx in range(1,11):
            test_set = f(idx)
            print("test_set idx is S", idx)
            train_set = get_Sc(idx).sample(n=int(TSS))
            print(len(train_set))
            common_words = get_most_commons(train_data, skip=100, total=1000)
            train_set = generate_vectors(train_data, common_words)  # Vectors have class labels at the end
            test_set = generate_vectors(test_data, common_words)
            actual_labels = np.array([row[-1] for row in test_set])
            print("Learning RF")
            model_idx = 1
            dt_loss[idx - 1] = learn_models(train_set, test_set, actual_labels, 1)
            model_idx = 2
            bt_loss[idx - 1] = learn_models(train_set, test_set, actual_labels, 2)
            model_idx = 3
            rf_loss[idx - 1] = learn_models(train_set, test_set, actual_labels, 3)
            model_idx = 4
            # print("Learning SVM")
            svm_loss[idx - 1] = learn_models(train_set, test_set, actual_labels, 4)        #SVM
        print("Calculating means")
        dt_av[i] = dt_loss.mean()
        dt_ste[i] = dt_loss.std()/sqrt(10)
        print(rf_loss,"<-- rf_loss")
        bt_av[i] = bt_loss.mean()
        bt_ste[i] = bt_loss.std()/sqrt(10)
        rf_av[i] = rf_loss.mean()
        rf_ste[i] = rf_loss.std()/sqrt(10)
        svm_av[i] = svm_loss.mean()
        svm_ste[i] = svm_loss.std()/sqrt(10)
        i += 1
        print("here")
    print("dt_av = ", dt_av)
    print("dt_ste = ", dt_ste)
    print("bt_av = ", bt_av)
    print("bt_ste = ", bt_ste)
    print("rf_av = ", rf_av)
    print("rf_ste = ", rf_ste)
    print("svm_av = ", svm_av)
    print("svm_ste = ", svm_ste)
    return None

def analysis2(yelp_data):
    global model_idx
    make_sets(yelp_data)
    # proportions = np.array([0.025, 0.05, 0.125, 0.25])
    # sizes = 2000 * proportions
    dt_av, dt_ste = list(), list()
    bt_av, bt_ste = list(), list()
    rf_av, rf_ste = list(), list()
    svm_av, svm_ste = list(), list()
    TSS = 500
    for number in [200, 500, 1000, 1500]:
        dt_loss = np.zeros(10, dtype=float)
        bt_loss = np.zeros(10, dtype=float)
        rf_loss = np.zeros(10, dtype=float)
        svm_loss = np.zeros(10, dtype=float)
        for idx in range(1, 11):
            test_set = f(idx)
            # # print("test_set idx is S", idx)
            train_set = get_Sc(idx).sample(n=int(TSS))
            # actual_labels = np.array([row[-1] for row in test_set])

            model_idx = 1
            dt_loss[idx - 1] = learn_models(train_set, test_set, 1, number)
            model_idx = 2
            bt_loss[idx - 1] = learn_models(train_set, test_set, 2, number)
            model_idx = 3
            # print("Learning RF")
            rf_loss[idx - 1] = learn_models(train_set, test_set, 3, number)
            model_idx = 4
            # # print("Learning SVM")
            svm_loss[idx - 1] = learn_models(train_set, test_set, 4, number)  # SVM
        # print("Calculating means")
        print("dt_loss = ", dt_loss)
        print("bt_loss = ", bt_loss)
        print("rf_loss = ", rf_loss)
        print("svm_loss = ", svm_loss)
        dt_av.append(dt_loss.mean())
        dt_ste.append(dt_loss.std() / sqrt(10))
        # print(rf_loss, "<-- rf_loss")
        bt_av.append(bt_loss.mean())
        bt_ste.append(bt_loss.std() / sqrt(10))
        rf_av.append(rf_loss.mean())
        rf_ste.append(rf_loss.std() / sqrt(10))
        svm_av.append(svm_loss.mean())
        svm_ste.append(svm_loss.std() / sqrt(10))
        # print("here")
    print("dt_av = ", dt_av)
    print("dt_ste = ", dt_ste)
    print("bt_av = ", bt_av)
    print("bt_ste = ", bt_ste)
    print("rf_av = ", rf_av)
    print("rf_ste = ", rf_ste)
    print("svm_av = ", svm_av)
    print("svm_ste = ", svm_ste)
    return None


def analysis3(yelp_data):
    global model_idx
    global maxdepth
    make_sets(yelp_data)
    # proportions = np.array([0.025, 0.05, 0.125, 0.25])
    # sizes = 2000 * proportions
    dt_av, dt_ste = list(), list()
    bt_av, bt_ste = list(), list()
    rf_av, rf_ste = list(), list()
    # svm_av, svm_ste = list(), list()
    TSS = 500
    for depth in [15, 20]:
        maxdepth = depth
        dt_loss = np.zeros(10, dtype=float)
        bt_loss = np.zeros(10, dtype=float)
        rf_loss = np.zeros(10, dtype=float)
        # svm_loss = np.zeros(10, dtype=float)
        for idx in range(1, 11):
            test_set = f(idx)
            print(idx)
            # # print("test_set idx is S", idx)
            train_set = get_Sc(idx).sample(n=int(TSS))
            model_idx = 1
            dt_loss[idx - 1] = learn_models(train_set, test_set, 1, 1000)
            model_idx = 2
            bt_loss[idx - 1] = learn_models(train_set, test_set, 2, 1000)
            model_idx = 3
            # print("Learning RF")
            rf_loss[idx - 1] = learn_models(train_set, test_set, 3, 1000)
            model_idx = 4
            # # print("Learning SVM")
            # svm_loss[idx - 1] = learn_models(train_set, test_set, 4, 1000)  # SVM
        # print("Calculating means")
        print("dt_loss = ", dt_loss)
        print("bt_loss = ", bt_loss)
        print("rf_loss = ", rf_loss)
        # print("svm_loss = ", svm_loss)
        dt_av.append(dt_loss.mean())
        dt_ste.append(dt_loss.std() / sqrt(10))
        # print(rf_loss, "<-- rf_loss")
        bt_av.append(bt_loss.mean())
        bt_ste.append(bt_loss.std() / sqrt(10))
        rf_av.append(rf_loss.mean())
        rf_ste.append(rf_loss.std() / sqrt(10))
        # svm_av.append(svm_loss.mean())
        # svm_ste.append(svm_loss.std() / sqrt(10))
        # print("here")
    print("dt_av = ", dt_av)
    print("dt_ste = ", dt_ste)
    print("bt_av = ", bt_av)
    print("bt_ste = ", bt_ste)
    print("rf_av = ", rf_av)
    print("rf_ste = ", rf_ste)
    # print("svm_av = ", svm_av)
    # print("svm_ste = ", svm_ste)
    return None
# Main
if __name__ == '__main__':
    global model_idx
    if len(sys.argv) == 4:
        train_data_file = sys.argv[1]
        test_data_file = sys.argv[2]
        model_idx = int(sys.argv[3])

        train_data = read_dataset(train_data_file)
        test_data = read_dataset(test_data_file)

        common_words = get_most_commons(train_data, skip=100, total=1000)
        train_vectors = generate_vectors(train_data, common_words) # Vectors have class labels at the end
        test_vectors = generate_vectors(test_data, common_words)
        actual_class_labels = np.array([row[-1] for row in test_vectors])
        learn_models(train_vectors, test_vectors, actual_class_labels, model_idx)
        if model_idx == 1:
            tree = learn_decision_trees(train_vectors)
            predicted_class_labels = apply_decision_tree(tree, test_vectors)
            print('ZERO-ONE-LOSS-DT', evaluate_loss(predicted_class_labels, actual_class_labels))
        elif model_idx == 2:
            trees = learn_bagging(train_vectors)
            predicted_class_labels = apply_bagging(trees, test_vectors)
            print('ZERO-ONE-LOSS-BT', evaluate_loss(predicted_class_labels, actual_class_labels))
        elif model_idx == 3:
            trees = learn_bagging(train_vectors)
            predicted_class_labels = apply_bagging(trees, test_vectors)
            print('ZERO-ONE-LOSS-RF', evaluate_loss(predicted_class_labels, actual_class_labels))
        else:
            print('Illegal modelIdx')

    else:
        # yelp_data = pd.read_csv('yelp_data.csv', sep='\t', header=None,
        #                         names=['reviewID', 'classLabel', 'reviewText'])  # read_dataset("yelp_data.csv")
        # review_text_yelp = pre_process(yelp_data["reviewText"])
        # yelp_data.drop(["reviewText"], inplace=True, axis=1)
        # yelp_data["reviewText"] = review_text_yelp
        # analysis2(yelp_data)
        print('usage: python hw4.py train.csv test.csv modelIdx')