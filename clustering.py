# import statements
import pandas as pd
import numpy as np
# from collections import Counter
import string
import sys
from numpy.linalg import norm
from math import sqrt
from math import log
from random import sample
from random import randrange
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist, pdist
from time import gmtime, strftime
from scipy.cluster.hierarchy import linkage, dendrogram, single, complete, average



def gray_scale(line, imagename):
    im = (line.reshape(28, 28))
    plt.gray()
    plt.imsave(imagename, im)


def exploration_1(raw_line, i):
    # print(raw_line.loc[:,1])
    image_names = ['e0.png','e1.png', 'e2.png', 'e3.png', 'e4.png', 'e5.png',
                   'e6.png', 'e7.png', 'e8.png', 'e9.png']
    line = np.array(raw_line.loc[:,2:])
    im = (line.reshape(28, 28))
    plt.gray()
    plt.imsave(image_names[i], np.real(im))


def exploration_2(data):
    rows = np.random.choice(data.index.values, 1000)
    sampled_df = data.ix[rows]
    # print(sampled_df)
    x = sampled_df[2]# x = [4, 8, 12, 16, 1, 4, 9, 16]
    y = sampled_df[3]# y = [1, 4, 9, 16, 4, 8, 12, 3]
    label = sampled_df[1]# label = [0, 1, 2, 3, 0, 1, 2, 3]
    plt.scatter(x, y, c=label)
    plt.show()


def visualisation_4(x, y, label):
    # rows = np.random.choice(data.index.values, 1000)
    # sampled_df = data.ix[rows]
    # # print(sampled_df)
    # x = sampled_df[2]# x = [4, 8, 12, 16, 1, 4, 9, 16]
    # y = sampled_df[3]# y = [1, 4, 9, 16, 4, 8, 12, 3]
    # label = sampled_df[1]# label = [0, 1, 2, 3, 0, 1, 2, 3]
    plt.scatter(x, y, c=label, s=6)
    plt.show()


def cluster_points(coordinates, centroids):
    clusters = {}
    # cluster_labels = {}
    # i = 0
    for p in coordinates:
        # print(p)
        point = p[1:].reshape(1,2)
        # print(point)
        norms = np.array([np.linalg.norm(point - c) for c in centroids])
        min_index = np.argmin(norms)
        if min_index in clusters:
            # clusters[min_index].append(point)
            clusters[min_index].append(p)
            # cluster_labels[min_index].append(l[i])
        else:
            clusters[min_index] = [p]
            # cluster_labels[min_index] = l[i]

    # print("clusters type", clusters)
    # print("labelss", cluster_labels)
    return clusters


def find_new_centroids(clusters):
    new = np.array([np.mean(clusters[i], axis=0) for i in clusters.keys()])
    # print(new)
    new = new[:,1:]
    # print("aaaa")
    return new


def find_centers(K_no):
    coordinates = embedding_data.as_matrix(columns=embedding_data.columns[1:])  # data points
    # labels = embedding_data.as_matrix(columns=embedding_data.columns[1:2])  # data points
    centroids = coordinates[np.random.choice(coordinates.shape[0], K_no, replace=False), :]
    # print(centroids)
    centroids = centroids[:,1:]
    # print(centroids)
    count = 0
    while count < 50:
    # centroids = new_centroids
        clusters = cluster_points(coordinates, centroids)
        centroids = find_new_centroids(clusters)
        # print(centroids)
        count += 1
    # print(centroids)
    return (centroids, clusters)


def get_wc_ssd(centroids, clusters):
    sum = 0
    for k,v in clusters.items():
        # print("k",k)
        # print("v", v)
        # print("c", centroids[k])
        for item in v:
            item = item[1:].reshape(1, 2)
            sum += (norm(item - centroids[k])**2)
    # print(sum)
    return sum


def get_nmi(clusters, class_labels):
    # print(clusters.type)
    # print("class labels\n", class_labels)
    N = class_labels.size
    class_labels = class_labels.reshape(1, N)
    indices = np.arange(0, np.max(class_labels) + 1)
    prob_class = np.array([np.count_nonzero(class_labels == i) for i in indices])
    prob_class = prob_class / N
    # print(prob_class.sum())
    prob_clusters = np.array([len(v) for k,v in clusters.items()])
    prob_clusters = prob_clusters / N
    # print(prob_clusters)
    sum = 0
    prob_c_g = np.zeros((prob_clusters.size,prob_class.size))
    j = 0
    for k,v in clusters.items():
        # print("v", v)
        v = np.array(v)
        # print(v)
        l = v[:,0]
        # print(l)
        # prob_c_g[i] = np.array([np.count_nonzero(class_labels == i) for i in indices])
        prob_c_g[j] = [np.count_nonzero(l == i) for i in indices]
        prob_c_g[j] = np.array(prob_c_g[j]) / N
        j += 1
    # print(prob_c_g)
    for i in range(0, prob_clusters.size):
        for j in range(0, prob_class.size):
            if prob_clusters[i]==0:
                prob_clusters[i]=1e-9
            if prob_class[j] == 0:
                prob_class[j] = 1e-9
            x = prob_c_g[i][j] / (prob_clusters[i] * prob_class[j])
            if x == 0:
                y = 1
            else:
                y = log(x)
            sum += prob_c_g[i][j] * y
    prob_class = prob_class * np.log(prob_class)
    sum_c = prob_class.sum()
    prob_clusters = prob_clusters * np.log(prob_clusters)
    sum_g = prob_clusters.sum()
    nmi = sum / (-sum_c - sum_g)
    # print(nmi)
    return nmi


def get_sc(clusters):
    S = list()
    for k,v in clusters.items():
        # distances_point_cluster(np.random.normal(size=(1,2)), v)
        distances_same_cluster_array = distances_same_cluster(v)
        v = np.array(v)
        # print(v)
        v = v[:, 1:]
        # print("now")
        # print(v)
        for i in range(len(v)):
            d = v[i].reshape(1, 2)
            #Find a
            a = distances_same_cluster_array[i].mean()
            # Find b's
            # b_array = np.zeros(len(clusters.keys()))
            # for k1, other in clusters.items():
            #     if k != k1):
            #         print(distances_point_cluster(d, other))
            b_array = np.array([distances_point_cluster(d, other) for k1, other in clusters.items() if k != k1 ])
            # print(b_array)
            b = b_array.min()
            S.append((b - a) / max(a,b))
    S = np.array(S)
    return S.mean()


def distances_same_cluster(cluster_v):
    v = np.array(cluster_v)
    v = v[:,1:]
    distances = np.zeros((v.shape[0], v.shape[0]))
    i = 0
    for point in v:
        point = point.reshape(1, 2)
        # print(point.shape)
        # print(v.shape)
        distances[i] = cdist(v, point).reshape(v.shape[0], )
        i += 1
    return distances


def distances_point_cluster(p, cluster_v):
    v = np.array(cluster_v)
    v = v[:, 1:]
    distances = cdist(v, p)
    # print(v.shape)
    # print(distances.shape) # (7,1)
    # print(distances)
    # distances = np.zeros((v.shape[0], v.shape[0]))
    # i = 0
    # for point in v:
    #     point = point.reshape(1, 2)
    #     # print(point.shape)
    #     # print(v.shape)
    #     distances[i] = cdist(v, point).reshape(v.shape[0], )
    #     i += 1
    return distances.mean()


def k_means_analysis():
    wc_ssd = list()
    sc_list = list()
    for k in [2, 4, 8, 16, 32]:
        cent, clust = find_centers(k)
        wc = get_wc_ssd(cent, clust)
        # print("WC-SSD ", wc)
        wc_ssd.append(wc)
        sc = get_sc(clust)
        # print("SC ", sc)
        sc_list.append(sc)
    print("wc = ",wc_ssd)
    print("sc = ",sc_list)
    return None


def k_means_analysis3():
    av_wc = list()
    var_wc = list()
    av_sc, var_sc = list(), list()
    for k in [2, 4, 8, 6, 32]:
        wc = np.zeros(10, dtype=float)
        sc = np.zeros(10, dtype=float)
        for i in range(0, 10):
            cent, clust = find_centers(k)
            wc[i]=get_wc_ssd(cent, clust)
            sc[i] = get_sc(clust)
        av_wc.append(wc.mean())
        av_sc.append(sc.mean())
        var_sc.append(sc.var())
        var_wc.append(wc.var())
    print("av_sc = ", av_sc)
    print("av_wc = ", av_wc)
    print("var_sc = ", var_sc)
    print("var_wc = ", var_wc)
    return None


def hierarchical_single(data):
    data_cov = data.ix[:,2:]
    d_matrix = pdist(data_cov)
    # z = single(d_matrix)
    # z = complete(d_matrix)
    z = average(d_matrix)
    dendrogram(z)
    plt.show()


def implement_pca():
    data = raw_data.as_matrix(columns=raw_data.columns[2:])
    # print(data)
    means = np.mean(data, axis=0)
    X = data - means
    # print(X)
    cov_mat = np.cov(X.T)
    # print(cov_mat.shape)
    eig_vals, eig_vecs = np.linalg.eig(cov_mat)
    sorted1 = np.argsort(eig_vals)[::-1]
    eigenvector = np.matrix(eig_vecs[:, sorted1])

    eigen_matrix = eigenvector[:, :10]
    print(eigen_matrix)
    X_new = np.dot(eigen_matrix.T, X.T)
    # X_new = np.dot( eigen_matrix.T, X)
    print(X_new.shape)

    # 2
    # print(eigen_matrix[:,0])
    eig = np.real(eigen_matrix)
    gray_scale(eig[:, 0].reshape(1, 784), 'pca0.png')
    gray_scale(eig[:, 1].reshape(1, 784), 'pca1.png')
    gray_scale(eig[:, 2].reshape(1, 784), 'pca2.png')
    gray_scale(eig[:, 3].reshape(1, 784), 'pca3.png')
    gray_scale(eig[:, 4].reshape(1, 784), 'pca4.png')
    gray_scale(eig[:, 5].reshape(1, 784), 'pca5.png')
    gray_scale(eig[:, 6].reshape(1, 784), 'pca6.png')
    gray_scale(eig[:, 7].reshape(1, 784), 'pca7.png')
    gray_scale(eig[:, 8].reshape(1, 784), 'pca8.png')
    gray_scale(eig[:, 9].reshape(1, 784), 'pca9.png')


if __name__ == '__main__':
    if len(sys.argv) == 3:
        data_file = sys.argv[1]
        K = int(sys.argv[2])
        # print("one split done:", strftime("%Y-%m-%d %H:%M:%S", gmtime()))
        # embedding_data_1 = pd.read_csv("digits-embedding-short2.csv", header=None)
        raw_data = pd.read_csv("digits-raw.csv", header=None)

        # for i in range(0,10):
        #     print(i)
        #     raw_data_i = raw_data.loc[(raw_data[1] == i)]
        #     exploration_1(raw_data_i.iloc[[randrange(len(raw_data_i))]], i)
        # exploration_1(raw_data.iloc[[randrange(len(raw_data))]])
        # exploration_2(embedding_data)

        embedding_data1 = pd.read_csv(data_file, header=None)
        embedding_data = embedding_data1
        # k_means_analysis()
        # k_means_analysis3()

        embedding_data_ii = embedding_data1.loc[(embedding_data1[1] == 2) | (embedding_data1[1] == 4) | (embedding_data1[1] == 6) | (embedding_data1[1] == 7)]
        # embedding_data = embedding_data_ii
        # k_means_analysis()
        # k_means_analysis3()

        embedding_data_iii = embedding_data1.loc[(embedding_data1[1] == 6) | (embedding_data1[1] == 7)]
        # embedding_data = embedding_data_iii
        # k_means_analysis()
        # k_means_analysis3()
        # implement_pca()

        cent, clust = find_centers(K)

        # Visualistaion for B4
        # x_values, y_values, clust_l = list(), list(), list()
        # for k,v in clust.items():
        #     count = 0
        #     for item in v:
        #         x_values.append(item[1:2])
        #         y_values.append(item[2:])
        #         clust_l.append(k)
        #         if count >= 125:
        #             break
        # visualisation_4(x_values, y_values, clust_l)

        wc =  get_wc_ssd(cent, clust)
        print("WC-SSD ", wc)

        sc = get_sc(clust)
        print("SC ", sc)

        nmi1 = get_nmi(clust, embedding_data.as_matrix(columns=embedding_data.columns[1:2]))
        print("NMI ", nmi1)

        # Plotting dendrograms
        # data = pd.DataFrame([])
        # for i in range(0, 10):
        #     sample1 = embedding_data1.loc[(embedding_data[1] == i)]
        #     data = pd.concat([data, sample1.sample(10)], join="outer" )
        # hierarchical_single(data)

        # print("one split done:", strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    else:
        print(len(sys.argv))
        print('usage: python hw5.py digits-embedding.csv K')