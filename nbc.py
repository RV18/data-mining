# Bag of Words - NBC
# import statements
import pandas as pd
import numpy as np
import string
import sys

# Function to preprocess the data
def pre_process(rText):
    newrText = []
    for i in range(len(rText)):
        newrText.append(str(rText[i]).lower())
        translator = str.maketrans('', '', string.punctuation)
        newrText[i] = newrText[i].translate(translator)
        newrText[i] = newrText[i].split()
    return newrText


# Function to find and count unique words
def count_unique_words(new_review_text):
    import collections
    c = collections.Counter()
    # print("Initial :", c)
    for review in new_review_text:  # TO DO - should newReviewText be populated with the no dup values?
        review_nodup = {word for word in review}
        c.update(review_nodup)
    # print("Final :", c)
    return c


# Function to sort and discard top n words
def sort_and_discard(wordFreqTable, n):
    # print("Before discarding top 100: \n", wordFreqTable)
    discard_count = 0
    for k, v in wordFreqTable.most_common():
        if discard_count < n:
            del wordFreqTable[k]
            discard_count += 1
    # print("After discarding top 100: \n",wordFreqTable)
    return wordFreqTable


# Function to construct features for next 500 words
def construct_features(features_set, wordFreqTable500, review_text_set):
    # feature_words = open('feature_words.txt', 'w')
    # print(wordFreqTable500)
    i, j = 0, 0
    for review in review_text_set:
        # print(type(review), review)
        for k, v in wordFreqTable500:
            if k in review:
                features_set[i, j] = 1
                # feature_words.write(k + " ")
            j += 1
        i += 1
        j = 0
        # feature_words.write("\n")
    # feature_words.close()
    return features_set


# Function to construct features for next 500 words
def construct_features_file(filename, features_set, wordFreqTable500, review_text_set, columns):
    # feature_words = open(filename, 'w')
    # print(wordFreqTable500)
    i, j = 0, 0
    for review in review_text_set:
        # print(type(review), review)
        for k, v in wordFreqTable500:
            if k in review:
                features_set[i, j] = 1
                # feature_words.write(k + " ")
            j += 1
        i += 1
        j = 0
        # feature_words.write("\n")
    # feature_words.close()
    return features_set


# Function - Write feature vector to file
def features_to_file(filename, features):
    target = open(filename, 'w')
    for line in features:
        target.write(" ".join(str(elem) for elem in line) + "\n")
    target.close()


# Function - print top ten selected words - words 101-110
def print_top_ten(wordFreqTable500):
    counter_ten = 1
    for k, v in wordFreqTable500:
        print("WORD%d %s" % (counter_ten, k))
        counter_ten += 1
        if counter_ten > 10:
            break
    return None


# Function to count number of positive, negative and total reviews in training data
def count_number_reviews(list_label):
    return sum(list_label), len(list_label) - sum(list_label), len(list_label)


# Function to calculate P(0) and P(1)
def calc_prior(list_label):
    return sum(list_label), len(list_label) - sum(list_label), len(list_label)


# Function - calculate likelihoods
def calc_likelihood(feature_vector, list_label, number_positive, number_negative):
    list_label = np.array(list_label)
    b = (list_label == 1)
    f_positive = np.array(feature_vector[b])
    b = (list_label == 0)
    f_negative = np.array(feature_vector[b])
    word_counts_positive = f_positive.sum(axis=0)
    word_counts_negative = f_negative.sum(axis=0)
    # print(word_counts_positive)
    # print(len(word_counts_positive))
    cdp_positive = (word_counts_positive + 1) / (number_positive + 2)  # with Laplace smoothing
    cdp_negative = (word_counts_negative + 1) / (number_negative + 2)
    return cdp_positive, cdp_negative


# Function - Calculate posterior prob and classify
def calc_classify(test_feature, cdp_positive, cdp_negative, prior_positive, prior_negative):
    predicted_class_labels = np.zeros(len(test_feature), dtype=np.int)
    i = 0
    for b in test_feature:
        a = (b * cdp_positive) + (1 - b) * (1 - cdp_positive)
        post_pos = np.prod(a) * prior_positive
        a = (b * cdp_negative) + (1 - b) * (1 - cdp_negative)
        post_neg = np.prod(a) * prior_negative
        if post_pos >= post_neg:            # TO DO - randomize equal case
            predicted_class_labels[i] = 1
        i += 1
    return predicted_class_labels


# Function - evaluate zero-one loss
def evaluate_loss(expected, predicted):
    diff = (expected == predicted)
    # print("No. of matches in expected and predicted: ", diff.sum())
    return 1 - (diff.sum() / len(expected))


def pick_random_sample(percent):
    a = np.arange(len(yelp_data))                                   # numbers from 0 to len
    percentage = percent / 100
    no_rows_int = int(percentage * len(yelp_data))                  # No of rows picked for training
    # print("No. of rows picked for training: ",no_rows_int)
    train_indices = np.random.choice(a, no_rows_int, replace=False) # Picking n ind randomly from a
    # print("train indices are ", train_indices)
    zero_array = np.zeros(len(yelp_data), dtype=bool)
    zero_array[train_indices] = True                                # Bool array with train ind set to true
    train = yelp_data[zero_array]                                   # Picking those for training
    test  = yelp_data[~zero_array]                                  # Picking remaining for testing
    # print("After random sampling: ", zero_array)
    # print("train data ", train)
    return train, test


def learn_NBC(number, columns, text_for_training, text_for_testing, train):
    wordTable = count_unique_words(text_for_training)                   # Counter
    wordTable = sort_and_discard(wordTable, 100)                        # Sort and discard top 100
    # Construct features for training set
    # columns = 500                                                       # most frequent words(101-600)
    rows = len(text_for_training)
    # print("No of rows in training set: ", rows)
    training_features = np.array([[0 for x in range(columns)] for y in range(rows)])
    wordTable500 = wordTable.most_common(columns)
    # print("Length of wordTable500 is ", len(wordTable500))
    # features_train = construct_features_file('feat_tr.txt', features_train, wordFreqTable500, review_text_train, columns)
    training_features = construct_features(training_features, wordTable500, text_for_training)
    # features_to_file('features_train.txt', features_train)
    if number == 'Q1':
        print_top_ten(wordTable500)   # TO DO - uncomment# Construct features for testing set
    rows = len(text_for_testing)
    # print("No of rows in testing set: ", rows)
    test_features = np.array([[0 for x in range(columns)] for y in range(rows)])
    # features_test = construct_features_file('feat_test.txt', features_test, wordFreqTable500, review_text_test, columns)
    test_features = construct_features(test_features, wordTable500, text_for_testing)
    # features_to_file('features_test.txt', features_test)
    number_positive, number_negative, number_total = count_number_reviews(train['classLabel'])
    # print(number_positive, number_negative, number_total)
    prior_positive = number_positive / number_total  # P(1)
    prior_negative = 1 - prior_positive  # P(0)
    # print("P(0) is %f and P(1) is %f" % (prior_positive, prior_negative))
    # Learn NBC - Calculate likelihoods
    cdp_positive, cdp_negative = calc_likelihood(training_features, train['classLabel'], number_positive, number_negative)
    return test_features, cdp_positive, cdp_negative, prior_positive, prior_negative


def question3():
    loss_avg = []
    loss_std_dev = []
    error_avg = []
    error_std_dev = []

    for pc in [1, 5, 10, 20, 50, 90]:
        zero_loss_array = []
        zero_loss_array_error =[]
        for repeat_counter in range(1, 11):
            training_data, testing_data = pick_random_sample(pc)
            # Learn NBC from training data
            features_test, likelihood_pos, likelihood_neg, prob_one, prob_zero = \
                learn_NBC("Q3", 500, training_data["reviewText"], testing_data["reviewText"], training_data)
            # Apply NBC to test data
            predicted_class_labels = calc_classify(features_test, likelihood_pos, likelihood_neg, prob_one, prob_zero)
            zero_one_loss = evaluate_loss(np.array(testing_data['classLabel']), predicted_class_labels)
            zero_loss_array.append(zero_one_loss)
            # Baseline error
            x = np.array(training_data["classLabel"])
            if np.count_nonzero(x) > x.size - np.count_nonzero(x):
                predicted_class_labels = np.ones(len(testing_data['classLabel']))     # ("Majority is 1")
            else:
                predicted_class_labels = np.zeros(len(testing_data['classLabel']))     # ("Majority is 0")
            zero_one_loss = evaluate_loss(np.array(testing_data['classLabel']), predicted_class_labels)
            zero_loss_array_error.append(zero_one_loss)
        zero_loss_array_error = np.array(zero_loss_array_error)
        zero_loss_array = np.array(zero_loss_array)
        loss_avg.append(np.mean(zero_loss_array))
        loss_std_dev.append(np.std(zero_loss_array))
        error_avg.append(np.mean(zero_loss_array_error))
        error_std_dev.append(np.std(zero_loss_array_error))
    print("Loss avgs :", loss_avg)
    print("Std avgs: ", loss_std_dev)
    print("Loss avgs for error:", error_avg)
    print("Std avgs: ", error_std_dev)

    return


def question4():
    loss_avg = []
    loss_std_dev = []
    error_avg = []
    error_std_dev = []

    for W in [10, 50, 250, 500, 1000, 4000]:
        zero_loss_array = []
        zero_loss_array_error = []
        for repeat_counter2 in range(1, 11):
            training_data, testing_data = pick_random_sample(50)
            # Learn NBC from training data
            features_test, likelihood_pos, likelihood_neg, prob_one, prob_zero = \
                learn_NBC("Q3", W, training_data["reviewText"], testing_data["reviewText"], training_data)
            # Apply NBC to test data
            predicted_class_labels = calc_classify(features_test, likelihood_pos, likelihood_neg, prob_one, prob_zero)
            zero_one_loss = evaluate_loss(np.array(testing_data['classLabel']), predicted_class_labels)
            zero_loss_array.append(zero_one_loss)
            # Baseline error
            x = np.array(training_data["classLabel"])
            if np.count_nonzero(x) > x.size - np.count_nonzero(x):
                predicted_class_labels = np.ones(len(testing_data['classLabel']))  # ("Majority is 1")
            else:
                predicted_class_labels = np.zeros(len(testing_data['classLabel']))  # ("Majority is 0")
            zero_one_loss = evaluate_loss(np.array(testing_data['classLabel']), predicted_class_labels)
            zero_loss_array_error.append(zero_one_loss)
        zero_loss_array_error = np.array(zero_loss_array_error)
        zero_loss_array = np.array(zero_loss_array)
        loss_avg.append(np.mean(zero_loss_array))
        loss_std_dev.append(np.std(zero_loss_array))
        error_avg.append(np.mean(zero_loss_array_error))
        error_std_dev.append(np.std(zero_loss_array_error))
    print("Loss avgs :", loss_avg)
    print("Std avgs: ", loss_std_dev)
    print("Loss avgs for error:", error_avg)
    print("Std avgs: ", error_std_dev)
    return


# Main
train_file = sys.argv[1]
test_file = sys.argv[2]
headers = ['reviewID', 'classLabel', 'reviewText']
training_data = pd.read_csv(train_file, sep='\t', header=None, names=headers)  # TO DO - add dtype as str?
testing_data = pd.read_csv(test_file, sep='\t', header=None, names=headers)  # TO DO - add dtype as str?

# Pre-process given training data
review_text_train = pre_process(training_data["reviewText"])
training_data.drop(["reviewText"], inplace=True, axis=1)
training_data["reviewText"] = review_text_train
# Pre-process given test data
review_text_test = pre_process(testing_data["reviewText"])
testing_data.drop(["reviewText"], inplace=True, axis=1)
testing_data["reviewText"] = review_text_test

# Learn NBC from training data
features_test, likelihood_pos, likelihood_neg, prob_one, prob_zero = \
    learn_NBC("Q1", 500, review_text_train, review_text_test, training_data)
# Apply NBC to test data
predicted_class_labels = calc_classify(features_test, likelihood_pos, likelihood_neg, prob_one, prob_zero)
zero_one_loss = evaluate_loss(np.array(testing_data['classLabel']), predicted_class_labels)
print("ZERO-ONE-LOSS ", zero_one_loss)      # DO NOT DELETE THIS PRINT STATEMENT

# yelp_data = pd.read_csv('yelp_data.csv', sep='\t', header=None, names=headers)
# # Pre-process given data from yelp_data.csv for Q3 and Q4
# review_text_yelp = pre_process(yelp_data["reviewText"])
# yelp_data.drop(["reviewText"], inplace=True, axis=1)
# yelp_data["reviewText"] = review_text_yelp
# # Q3a
# question3()
# # Q4a
# question4()
