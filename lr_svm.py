# Bag of Words - NBC
# import statements
import pandas as pd
import numpy as np
import string
import sys
from numpy import linalg as LA
import math

# np.set_printoptions(threshold=sys.maxsize)

S1 = pd.DataFrame([])
S2 = pd.DataFrame([])
S3 = pd.DataFrame([])
S4 = pd.DataFrame([])
S5 = pd.DataFrame([])
S6 = pd.DataFrame([])
S7 = pd.DataFrame([])
S8 = pd.DataFrame([])
S9 = pd.DataFrame([])
S10 = pd.DataFrame([])

# Function to preprocess the data
def pre_process(rText):
    newrText = []
    for i in range(len(rText)):
        newrText.append(str(rText[i]).lower())
        translator = str.maketrans('', '', string.punctuation)
        newrText[i] = newrText[i].translate(translator)
        newrText[i] = newrText[i].split()
    return newrText


# Function to find and count unique words
def count_unique_words(new_review_text):
    import collections
    c = collections.Counter()
    # print("Initial :", c)
    for review in new_review_text:  # TO DO - should newReviewText be populated with the no dup values?
        review_nodup = {word for word in review}
        c.update(review_nodup)
    # print("Final :", c)
    return c


# Function to sort and discard top n words
def sort_and_discard(wordFreqTable, n):
    # print("Before discarding top 100: \n", wordFreqTable)
    discard_count = 0
    for k, v in wordFreqTable.most_common():
        if discard_count < n:
            del wordFreqTable[k]
            discard_count += 1
    # print("After discarding top 100: \n",wordFreqTable)
    return wordFreqTable


# Function to construct features for next 500 words
def construct_features(features_set, wordFreqTable500, review_text_set, model):
    # feature_words = open('feature_words.txt', 'w')
    # print(wordFreqTable500)
    i, j = 0, 0
    for review in review_text_set:
        # print(type(review), review)
        for k, v in wordFreqTable500:
            if k in review:
                # if review.count(k) == 1:
                features_set[i, j] = 1
                # else:
                #     features_set[i, j] = 2
                # feature_words.write(k + " ")
            j += 1
        i += 1
        j = 0
        # feature_words.write("\n")
    # feature_words.close()
    if model != 0:                                     # Adding intercept for LR and SVM
        intercept = np.ones((features_set.shape[0], 1))
        features = np.hstack((intercept, features_set))
    return features_set


# Function to construct features for next 500 words
def construct_features_file(filename, features_set, wordFreqTable500, review_text_set, columns):
    # feature_words = open(filename, 'w')
    # print(wordFreqTable500)
    i, j = 0, 1  # First cell (i, 0) is for intercept
    for review in review_text_set:
        features_set[i, 0] = 1
        # print(type(review), review)
        for k, v in wordFreqTable500:
            if k in review:
                features_set[i, j] = 1
                # feature_words.write(k + " ")
            j += 1
        i += 1
        j = 1
        # feature_words.write("\n")
    # feature_words.close()
    return features_set


# Function - Write feature vector to file
def features_to_file(filename, features):
    target = open(filename, 'w')
    for line in features:
        target.write(" ".join(str(elem) for elem in line) + "\n")
    target.close()


# Function - print top ten selected words - words 101-110
def print_top_ten(wordFreqTable500):
    counter_ten = 1
    for k, v in wordFreqTable500:
        print("WORD%d %s" % (counter_ten, k))
        counter_ten += 1
        if counter_ten > 10:
            break
    return None


# Function to count number of positive, negative and total reviews in training data
def count_number_reviews(list_label):
    return sum(list_label), len(list_label) - sum(list_label), len(list_label)


# Function to calculate P(0) and P(1)
def calc_prior(list_label):
    return sum(list_label), len(list_label) - sum(list_label), len(list_label)


# Function - calculate likelihoods
def calc_likelihood(feature_vector, list_label, number_positive, number_negative):
    list_label = np.array(list_label)
    b = (list_label == 1)
    f_positive = np.array(feature_vector[b])
    b = (list_label == 0)
    f_negative = np.array(feature_vector[b])
    word_counts_positive = f_positive.sum(axis=0)
    word_counts_negative = f_negative.sum(axis=0)
    # print(word_counts_positive)
    # print(len(word_counts_positive))
    cdp_positive = (word_counts_positive + 1) / (number_positive + 2)  # with Laplace smoothing
    cdp_negative = (word_counts_negative + 1) / (number_negative + 2)
    return cdp_positive, cdp_negative


def calc_likelihood_analysis(feature_vector, list_label, number_positive, number_negative):
    list_label = np.array(list_label)
    b = (list_label == 1)
    f_positive = np.array(feature_vector[b])
    b = (list_label == 0)
    f_negative = np.array(feature_vector[b])
    word_counts_2 = np.zeros(len(feature_vector[0]))
    word_counts_1 = np.zeros(len(feature_vector[0]))
    word_counts_2n = np.zeros(len(feature_vector[0]))
    word_counts_1n = np.zeros(len(feature_vector[0]))
    for j in range(0, len(feature_vector[0])):
        # print(j)
        # print(word_counts_2[j])
        # print(f_positive[:, j])
        word_counts_2[j] = np.count_nonzero(f_positive[:, j] == 2)
        word_counts_1[j] = np.count_nonzero(f_positive[:, j] == 1)
        word_counts_2n[j] = np.count_nonzero(f_negative[:, j] == 2)
        word_counts_1n[j] = np.count_nonzero(f_negative[:, j] == 1)
    # for j in range(3):
    #     word_counts_2[j] = np.count_nonzero(a[:, j] == 2)
    #     word_counts_1[j] = np.count_nonzero(a[:, j] == 1)
        # zero[j] = np.subtract(5, (two[j] + one[j]))
    cpd_1p = (word_counts_1 + 1) / (number_positive + 3)
    cpd_2p = (word_counts_2 + 1) / (number_positive + 3)
    cpd_1n = (word_counts_1n + 1) / (number_negative + 3)
    cpd_2n = (word_counts_2n + 1) / (number_negative + 3)
    return cpd_1p, cpd_2p, cpd_1n, cpd_2n

# Function - Calculate posterior prob and classify
def calc_classify(test_feature, cdp_positive, cdp_negative, prior_positive, prior_negative):
    predicted_class_labels = np.zeros(len(test_feature), dtype=np.int)
    i = 0
    for b in test_feature:
        a = (b * cdp_positive) + (1 - b) * (1 - cdp_positive)
        post_pos = np.prod(a) * prior_positive
        a = (b * cdp_negative) + (1 - b) * (1 - cdp_negative)
        post_neg = np.prod(a) * prior_negative
        if post_pos >= post_neg:            # TO DO - randomize equal case
            predicted_class_labels[i] = 1
        i += 1
    return predicted_class_labels


def calc_classify_analysis(test_feature, cdp_1p, cdp_2p, cdp_1n, cdp_2n, prior_positive, prior_negative):
    predicted_class_labels = np.zeros(len(test_feature), dtype=np.int)
    i = 0
    from copy import deepcopy
    for b in test_feature:
        d = np.array(deepcopy(b))
        d[d!=1] = 0
        c = np.array(deepcopy(b))
        c[c!=2] = 0
        e = 1 - d - c
        e[e < 0] = 0
        a = (d * cdp_1p) + (c * cdp_2p) + e * (1 - cdp_1p - cdp_2p)
        post_pos = np.prod(a) * prior_positive
        a = (d * cdp_1n) + (c * cdp_2n) + e * (1 - cdp_1n - cdp_2n)
        post_neg = np.prod(a) * prior_negative
        if post_pos >= post_neg:            # TO DO - randomize equal case
            predicted_class_labels[i] = 1
        i += 1
    return predicted_class_labels

# Function - evaluate zero-one loss
def evaluate_loss(expected, predicted):
    diff = (expected == predicted)
    # print("No. of mismatches in expected and predicted: ", len(expected)- diff.sum())
    return 1 - (diff.sum() / len(expected))


# def pick_random_sample(percent):
#     a = np.arange(len(yelp_data))                                   # numbers from 0 to len
#     percentage = percent / 100
#     no_rows_int = int(percentage * len(yelp_data))                  # No of rows picked for training
#     # print("No. of rows picked for training: ",no_rows_int)
#     train_indices = np.random.choice(a, no_rows_int, replace=False) # Picking n ind randomly from a
#     # print("train indices are ", train_indices)
#     zero_array = np.zeros(len(yelp_data), dtype=bool)
#     zero_array[train_indices] = True                                # Bool array with train ind set to true
#     train = yelp_data[zero_array]                                   # Picking those for training
#     test  = yelp_data[~zero_array]                                  # Picking remaining for testing
#     # print("After random sampling: ", zero_array)
#     # print("train data ", train)
#     return train, test


def change_label_to_minus(y_actual):
    y_labels = np.array(y_actual)
    y_labels[y_labels < 1] = -1
    return y_labels

def learn_model_analysis(train_set, test_set, model):
    features_train, features_test = common_to_models(4000, train_set["reviewText"], test_set["reviewText"], model)
    likelihood_1p, likelihood_2p, likelihood_1n, likelihood_2n, prob_one, prob_zero = learn_NBC_analysis(features_train, features_test, train_set)
    predicted_class_labels = calc_classify_analysis(features_test, likelihood_1p, likelihood_2p, likelihood_1n, likelihood_2n, prob_one, prob_zero)
    zero_one_loss_nbc = evaluate_loss(np.array(test_set['classLabel']), predicted_class_labels)
    print("ZERO-ONE-LOSS-NBC ", zero_one_loss_nbc)
    return zero_one_loss_nbc


def learn_models(train_set, test_set, model):
    features_train, features_test = common_to_models(4000, train_set["reviewText"], test_set["reviewText"], model)
    if model is 0:
        likelihood_pos, likelihood_neg, prob_one, prob_zero = learn_NBC(features_train, features_test, train_set)
        predicted_class_labels = calc_classify(features_test, likelihood_pos, likelihood_neg, prob_one, prob_zero)
        zero_one_loss_nbc = evaluate_loss(np.array(test_set['classLabel']), predicted_class_labels)
        print("ZERO-ONE-LOSS-NBC ", zero_one_loss_nbc)
        return zero_one_loss_nbc
    elif model is 1:
        weights = learn_LR("Q1", len(features_train[0]), features_train, train_set['classLabel'])
        # print(weights)
        predicted_class_labels = apply_LR(features_test, weights)
        zero_one_loss_lr = evaluate_loss(np.array(test_set['classLabel']), predicted_class_labels)
        print("ZERO-ONE-LOSS-LR ", zero_one_loss_lr)
        return zero_one_loss_lr
    elif model is 2:
        weights = learn_SVM("Q1", len(features_train[0]), features_train, change_label_to_minus(train_set['classLabel']))
        # print(weights)
        predicted_class_labels = apply_SVM(features_test, weights)
        zero_one_loss_svm = evaluate_loss(change_label_to_minus(test_set['classLabel']), predicted_class_labels)
        print("ZERO-ONE-LOSS-SVM  ", zero_one_loss_svm)
        return zero_one_loss_svm
    else:
        print("Invalid model index value: ", modelIdx)
        return 0.00

def common_to_models(columns, text_for_training, text_for_testing, model):
    wordTable = count_unique_words(text_for_training)   # Counter
    wordTable = sort_and_discard(wordTable, 100)        # Sort and discard top 100
    columns += 1                                        # No of features - added 1 for intercept
    rows = len(text_for_training)
    wordTable500 = wordTable.most_common(columns)
    training_features = np.array([[0 for x in range(columns)] for y in range(rows)])
    training_features = construct_features(training_features, wordTable500, text_for_training, model)
    rows = len(text_for_testing)
    test_features = np.array([[0 for x in range(columns)] for y in range(rows)])
    test_features = construct_features(test_features, wordTable500, text_for_testing, model)
    # print(training_features, test_features)
    return training_features, test_features


def learn_LR(number, columns, training_features, y_labels):
    w = np.zeros(columns, dtype=np.longfloat)
    w_new = np.zeros(columns, dtype=np.longfloat)
    iterations = 0
    tol = 0
    while iterations < 100:
        product = np.dot(training_features, w)
        # Make predictions
        y_predictions = 1 / (1 + np.exp(-product))
        y_predictions[y_predictions >= 0.5] = 1
        y_predictions[y_predictions < 0.5] = 0
        # Calculate gradient
        for j in range(0, columns):
            s = 0
            w[j] = w_new[j]
            diff =  (np.array(y_labels) - y_predictions)
            s = diff * training_features[:,j]
            sum = s.sum(dtype=np.longfloat)
            gradient = sum - (0.01 * w[j] )                                     # Lambda    = 0.01
            w_new[j] = w[j] + (0.01 * gradient)
        tol = LA.norm(w_new - w)                                              # L2 norm
        if tol <= 0.000001:                                                   # Check tolerance
            break
        iterations += 1
    return w_new


def apply_LR(test_features, w):
    # print(test_features.shape)
    # print(w.shape)
    product = np.dot(test_features, w)
    # Make predictions
    y_predictions = 1 / (1 + np.exp(-product))
    y_predictions[y_predictions >= 0.5] = 1
    y_predictions[y_predictions < 0.5] = 0
    return y_predictions


def learn_SVM(number, columns, training_features, y_labels):
    w = np.zeros(columns, dtype=np.longfloat)
    N = len(training_features)
    gradient_2d = np.zeros((N, columns), dtype=np.longfloat)
    w_new = np.zeros(columns, dtype=np.longfloat)
    gradient = np.zeros(columns, dtype=np.longfloat)

    iterations = 0
    while iterations < 100:
        y_predictions = np.dot(training_features, w)
        # Make predictions
        y_predictions[y_predictions >= 0] = 1
        y_predictions[y_predictions < 0] = -1
        # Calculate gradient
        y_product = y_labels * y_predictions
        indices = np.where(y_product >= 1)[0]
        # print(indices)
        for j in range(0, columns):
            s = 0
            w[j] = w_new[j]
            # for i in indices:
            #     gradient_2d[i,j] = y_labels[i] * training_features[i, j]
            gradient_2d[:,j] = y_labels * training_features[:,j]
            for i in indices:
                gradient_2d[i,j] = 0
            sum = gradient_2d[:,j].sum()
            gradient = (0.01 * w[j]) - (sum / N)
            w_new[j] = w[j] - 0.5 * gradient
        tol = LA.norm(w_new - w)                                              # L2 norm
        # print("tol is ", tol)
        if tol <= 0.000001:                                                   # Check tolerance
            # print("am i here?")
            break
        iterations += 1
    # print("No of iterations ", iterations)
    # print(w_new)
    return w_new


def apply_SVM(test_features, w):
    # print(test_features.shape)
    # print(w.shape)
    y_predictions = np.dot(test_features, w)
    # Make predictions
    y_predictions[y_predictions >= 0] = 1
    y_predictions[y_predictions < 0] = -1
    return y_predictions


def learn_NBC(training_features, test_features, train):
    number_positive, number_negative, number_total = count_number_reviews(train['classLabel'])
    prior_positive = number_positive / number_total                     # P(1)
    prior_negative = 1 - prior_positive                                 # P(0)
    # Learn NBC - Calculate likelihoods
    cdp_positive, cdp_negative = calc_likelihood(training_features, train['classLabel'], number_positive, number_negative)
    return cdp_positive, cdp_negative, prior_positive, prior_negative


def learn_NBC_analysis(training_features, test_features, train):
    number_positive, number_negative, number_total = count_number_reviews(train['classLabel'])
    prior_positive = number_positive / number_total  # P(1)
    prior_negative = 1 - prior_positive  # P(0)
    # Learn NBC - Calculate likelihoods
    cpd_1p, cpd_2p, cpd_1n, cpd_2n = calc_likelihood_analysis(training_features, train['classLabel'], number_positive,
                                                 number_negative)
    return cpd_1p, cpd_2p, cpd_1n, cpd_2n, prior_positive, prior_negative


def partition_into_disjoint_sets(dataset):
    # np.random.shuffle(dataset)
    # subset = dataset.sample(frac=0.8)
    # remaining = dataset.drop(subset['reviewID'])
    a = np.arange(len(dataset))                                         # numbers from 0 to len
    no_rows_int = 200                                                   # No of rows picked for a set S
    subset_indices = np.random.choice(a, no_rows_int, replace=False)    # Picking n ind randomly from a
    zero_array = np.zeros(len(dataset), dtype=bool)
    zero_array[subset_indices] = True                                   # Bool array with train ind set to true
    subset = dataset[zero_array]                                        # Picking those for training
    remaining = dataset[~zero_array]                                    # Picking remaining
    return subset, remaining

def f(x):
    return {
        1: S1,
        2: S2,
        3: S3,
        4: S4,
        5: S5,
        6: S6,
        7: S7,
        8: S8,
        9: S9,
        10: S10,
    }[x]

def get_Sc(idx):
    Sc = pd.DataFrame([])
    for i in range(1, 11):
        if i != idx:
            Sc = Sc.append(f(i))
    return Sc

def make_sets(yelp_data):
    # Partition dataset into 10 disjoint sets
    global S1, S2, S3, S4, S5, S6, S7, S8, S9, S10
    S1, remaining_main = partition_into_disjoint_sets(yelp_data)
    S2, remaining_main = partition_into_disjoint_sets(remaining_main)
    S3, remaining_main = partition_into_disjoint_sets(remaining_main)
    S4, remaining_main = partition_into_disjoint_sets(remaining_main)
    S5, remaining_main = partition_into_disjoint_sets(remaining_main)
    S6, remaining_main = partition_into_disjoint_sets(remaining_main)
    S7, remaining_main = partition_into_disjoint_sets(remaining_main)
    S8, remaining_main = partition_into_disjoint_sets(remaining_main)
    S9, remaining_main = partition_into_disjoint_sets(remaining_main)
    S10, remaining_main = partition_into_disjoint_sets(remaining_main)


def analysis1(yelp_data):
    make_sets(yelp_data)
    proportions = np.array([0.01, 0.03, 0.05, 0.08, 0.1, 0.15])
    sizes = 2000 * proportions
    # print(sizes)
    nbc_av = np.zeros(6, dtype=float)
    nbc_ste = np.zeros(6, dtype=float)
    lr_av = np.zeros(6, dtype=float)
    lr_ste = np.zeros(6, dtype=float)
    svm_av = np.zeros(6, dtype=float)
    svm_ste = np.zeros(6, dtype=float)
    i = 0
    for TSS in sizes:
        nbc_loss = np.zeros(10, dtype=float)
        lr_loss = np.zeros(10, dtype=float)
        svm_loss = np.zeros(10, dtype=float)
        for idx in range(1,11):
            test_set = f(idx)
            # print("test_set idx is S", idx)
            train_set = get_Sc(idx).sample(n=int(TSS))
            # print(len(train_set))
            nbc_loss[idx-1] = learn_models(train_set, test_set, 0)        #NBC
            lr_loss[idx-1] = learn_models(train_set, test_set, 1)         #LR
            svm_loss[idx-1] = learn_models(train_set, test_set, 2)        #SVM
        nbc_av[i] = nbc_loss.mean()
        nbc_ste[i] = nbc_loss.std()/math.sqrt(10)
        lr_av[i] = lr_loss.mean()
        lr_ste[i] = lr_loss.std()/math.sqrt(10)
        svm_av[i] = svm_loss.mean()
        svm_ste[i] = svm_loss.std()/math.sqrt(10)
        i += 1
    print(nbc_av, nbc_ste)
    print(lr_av, lr_ste)
    print(svm_av, svm_ste)
    return None


def analysis2(yelp_data):
    make_sets(yelp_data)
    proportions = np.array([0.01, 0.03, 0.05, 0.08, 0.1, 0.15])
    sizes = 2000 * proportions
    # print(sizes)
    nbc_av = np.zeros(6, dtype=float)
    nbc_ste = np.zeros(6, dtype=float)
    lr_av = np.zeros(6, dtype=float)
    lr_ste = np.zeros(6, dtype=float)
    svm_av = np.zeros(6, dtype=float)
    svm_ste = np.zeros(6, dtype=float)
    i = 0
    for TSS in sizes:
        nbc_loss = np.zeros(10, dtype=float)
        lr_loss = np.zeros(10, dtype=float)
        svm_loss = np.zeros(10, dtype=float)
        for idx in range(1,11):
            test_set = f(idx)
            # print("test_set idx is S", idx)
            train_set = get_Sc(idx).sample(n=int(TSS))
            # print(len(train_set))
            nbc_loss[idx-1] = learn_model_analysis(train_set, test_set, 0)        #NBC
            lr_loss[idx-1] = learn_models(train_set, test_set, 1)         #LR
            svm_loss[idx-1] = learn_models(train_set, test_set, 2)        #SVM
        nbc_av[i] = nbc_loss.mean()
        nbc_ste[i] = nbc_loss.std()/math.sqrt(10)
        lr_av[i] = lr_loss.mean()
        lr_ste[i] = lr_loss.std()/math.sqrt(10)
        svm_av[i] = svm_loss.mean()
        svm_ste[i] = svm_loss.std()/math.sqrt(10)
        i += 1
    print(nbc_av, nbc_ste)
    print(lr_av, lr_ste)
    print(svm_av, svm_ste)
    return None


# Main
train_file = sys.argv[1]
test_file = sys.argv[2]
modelIdx = int(sys.argv[3])
headers = ['reviewID', 'classLabel', 'reviewText']
training_data = pd.read_csv(train_file, sep='\t', header=None, names=headers)  # TO DO - add dtype as str?
testing_data = pd.read_csv(test_file, sep='\t', header=None, names=headers)  # TO DO - add dtype as str?

# Pre-process given training data
review_text_train = pre_process(training_data["reviewText"])
training_data.drop(["reviewText"], inplace=True, axis=1)
training_data["reviewText"] = review_text_train
# Pre-process given test data
review_text_test = pre_process(testing_data["reviewText"])
testing_data.drop(["reviewText"], inplace=True, axis=1)
testing_data["reviewText"] = review_text_test

learn_models(training_data, testing_data, modelIdx)

# yelp_data = pd.read_csv('yelp_data.csv', sep='\t', header=None, names=headers)
# review_text_yelp = pre_process(yelp_data["reviewText"])
# yelp_data.drop(["reviewText"], inplace=True, axis=1)
# yelp_data["reviewText"] = review_text_yelp
# analysis1(yelp_data)
# analysis2(yelp_data)